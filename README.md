# Description

A Docker container for running Tor relay.

# Install

1| Build a stretch base image.

```
emerge debootstrap
debootstrap stretch stretch > /dev/null
tar -C stretch -c . | docker import - monkeysareevil/stretch
```

2| Build the container. Check the ENV's; you'll need to pick a nickname. Also consider filling out the commented out ENV's but these are optional.

```
docker build -t tor-relay.
```

3| Run it. You can run as a bridge, middle, or exit relay, just pick the right torrc. Make sure you know what you're getting into if you run an exit node (check the official docs).

```
docker run -v /etc/localtime:/etc/localtime --restart always -p 9001:9001 --name tor-relay tor-relay -f /etc/tor/torrc.bridge
```

# Licence

GPL v3+

FROM monkeysareevil/stretch

ENV NICKNAME=nickname
#ENV CONTACT_GPG_FINGERPRINT=####
#ENV CONTACT_NAME=example
#ENV CONTACT_EMAIL=tor@example.com

# Update and install tor
RUN apt update && apt -y upgrade && apt install -y tor

# default port to used for incoming Tor connections
# can be changed by changing 'ORPort' in torrc
EXPOSE 9001

# copy in our torrc files
COPY torrc.bridge /etc/tor/torrc.bridge
COPY torrc.exit /etc/tor/torrc.exit
COPY torrc.middle /etc/tor/torrc.middle

# add tor user
RUN groupadd tor
RUN useradd tor -m -g tor

# make sure files are owned by tor user
RUN chown -R tor /etc/tor

USER tor

ENTRYPOINT [ "tor" ]
